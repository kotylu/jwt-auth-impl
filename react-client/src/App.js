import React, {useState} from "react";
import cookies from "js-cookies";
import jwt from "jwt-decode";
import AdminAccess from "./AdminAccess";

function App() {

  const serverPort = 5000;
  const serverUrl = `http://localhost:${serverPort}`;

  const [registerForm, setRegisterForm] = useState({
    name: "",
    passwd: ""
  });

  const [loginForm, setLoginForm] = useState({
    name: "",
    passwd: ""
  });

  const [user, setUser] = useState({
    name: "",
    roles: []
  });

  const [appAdminData, setAppAdminData] = useState({
    allUsers: []
  });

  const log = () => {
    console.log("register: ", registerForm);
    console.log("login: ", loginForm);
    console.log("user: ", user);
    console.log("admin data: ", appAdminData);
  };

  const registerNameChange = (e) => {
    const updatedRegisterForm = {
      ...registerForm,
      name: e.target.value
    };
    setRegisterForm(updatedRegisterForm);
  };

  const registerPasswdChange = (e) => {
    const updatedRegisterForm = {
      ...registerForm,
      passwd: e.target.value
    };
    setRegisterForm(updatedRegisterForm);
  };

  const loginNameChange = (e) => {
    const updatedLoginForm = {
      ...loginForm,
      name: e.target.value
    };
    setLoginForm(updatedLoginForm);
  };

  const loginPasswdChange = (e) => {
    const updatedLoginForm = {
      ...loginForm,
      passwd: e.target.value
    };
    setLoginForm(updatedLoginForm);
  };

  const register = (e) => {
    e.preventDefault();
    (async() => {
      const req = {
          method: "POST",
          credentials: "include",
          headers: {
              contentType: "application/json"
          },
          body: JSON.stringify(registerForm)
      };
      const response = await fetch(`${serverUrl}/user/register`, req);
      console.log(response);
      updateUserFromJWT();
    })();
  };

  const login = (e) => {
    e.preventDefault();
    (async() => {
      const req = {
          method: "POST",
          credentials: "include",
          headers: {
              accept: "application/json",
              contentType: "application/json"
          },
          body: JSON.stringify(loginForm)
      };
      const response = await fetch(`${serverUrl}/user/login`, req);   
      console.log(response);
      updateUserFromJWT();
    })();
  };


  const updateUserFromJWT = () => {
    const jwtToken = cookies.getItem("my-token");
    if (!jwtToken) {
      return;
    }
    const decoded = jwt(jwtToken);
    console.log(decoded);
    setUser({
      name: decoded.username,
      roles: decoded.role
    });
  };

  const getAllUsers = (e) => {
    e.preventDefault();
    (async() => {
      const req = {
          method: "GET",
          credentials: "include",
      };
      const response = await fetch(`${serverUrl}/admin/getusers`, req)
      if (!response || !response.ok) {
        setAppAdminData({
          ...appAdminData,
          allUsers: []
        });
        return;
      }
      response.json()
      .then(data => {
        setAppAdminData({
          ...appAdminData,
          allUsers: data
        })
      })
      .catch(err => {
        console.error(err);
        setAppAdminData({
          ...appAdminData,
          allUsers: []
        });
      });
    })();
  };

  return (
    <div className="App">
      <h3>REGISTER</h3>
      <form>
        <input type="text" onChange={(e) => {registerNameChange(e);}} value={registerForm.name}/>
        <input type="text" onChange={(e) => {registerPasswdChange(e);}} value={registerForm.passwd}/>
        <button type="submit" onClick={(e) => {register(e);}}>Register</button>
      </form>
      
    <h3>LOGIN</h3>
    <form>
      <input type="text" onChange={(e) => {loginNameChange(e);}} value={loginForm.name}/>
      <input type="text" onChange={(e) => {loginPasswdChange(e);}} value={loginForm.passwd}/>
      <button type="submit" onClick={(e) => {login(e);}}>Login</button>
    </form>

    <button type="button" onClick={(e) => {log();}}>LOG</button>

    <h1>{user.name}</h1>

    <AdminAccess user={user}>
      <button type="button" onClick={(e) => {getAllUsers(e);}}>all users</button>
      <ul>
      {
        appAdminData.allUsers.map(((usr, ix) => (
          <li key={ix}>{usr}</li>
        )))
      }
      </ul>
    </AdminAccess>
    

    </div>
  );
}

export default App;
