import React from "react";

const AdminAccess = (props) => {
  return (
    <div style={{"display": props.user.roles.includes("admin") ? "block" : "none"}}>
      <h3>Admin Access</h3>
      {
        props.children
      }
    </div>
  );
};

export default AdminAccess;