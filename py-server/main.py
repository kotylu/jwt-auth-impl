from flask import Flask, request, url_for, redirect, make_response, session, request
from flask_cors import CORS, cross_origin
from authlib.jose import JsonWebToken
from argon2 import PasswordHasher
from datetime import datetime, timedelta
import json

app = Flask(__name__)
CORS(app, supports_credentials=True, cross_origins=["https://localhost:3000/"])
jwt = JsonWebToken(["HS256"])
jwt_key = "sup3r-s3cr3t-k3y"
passwdHasher = PasswordHasher()

users = {} # simulate DB

def make_cors_response():
    res = make_response()
    #res.headers.add("Access-Control-Allow-Origin", "*")
    return res

def get_jwt_token(user):
    current_date = datetime.now()
    expiration_date = current_date + timedelta(minutes=5)
    exp = int(datetime.timestamp(expiration_date)) # cut away milliseconds
    header = { "alg": "HS256" }
    payload = { "iss": "issuer@me", "sub":"subject@none", "exp": exp, "username":  user["name"], "role": ["admin"] if user["name"] == "lukas" else [] } # roles would be stored in DB as well
    return jwt.encode(header, payload, jwt_key)


def set_cookie_token(resp, jwt_token):
    raw_token = jwt.decode(jwt_token, jwt_key)
    resp.set_cookie("my-token", jwt_token, expires=raw_token["exp"])

def log(txt):
    print(f"LOG: {txt}")

def get_passwd(user):
    return f'${user["passwd"]}${user["name"]}'

def get_passwd_hash(user):
    return passwdHasher.hash(get_passwd(user)) 

@app.route("/")
def home():
    return "Hello World!";

@app.route("/user/login", methods=["POST"])
def login():
    if request.method == "POST":
        data = request.get_json(force=True)
        hash = users[data["name"]]
        passwdHasher.verify(hash, get_passwd(data)) # throws Exception
        # rehash password if outdated
        if passwdHasher.check_needs_rehash(hash):
            users[data["name"]] = passwdHasher.hash(get_passwd(data))

        jwt_token = get_jwt_token({"name": data["name"]})

        resp = make_cors_response()
        set_cookie_token(resp, jwt_token)
        return resp
    else:
        return "Invalid Method"

@app.route("/user/register", methods=["POST"])
def register():
    if request.method == "POST":
        data = request.get_json(force=True)
        users[data["name"]] = get_passwd_hash(data) # those data would be stored in DB

        jwt_token = get_jwt_token({"name": data["name"]})

        resp = make_cors_response()
        set_cookie_token(resp, jwt_token)

        return resp
    else:
        return "InvalidMethod"

@app.route("/admin/getusers") # for logged in users only
def all_users():
    token = request.cookies.get("my-token")
    claims = jwt.decode(token, jwt_key)
    claims.validate() # throws error when invalid
    if "admin" not in claims["role"]:
        res = make_cors_response()
        res.status_code = 500
        res.set_data("{ \"msg\": \" fuck you\" }")
        return res

    res = make_cors_response()
    res_body = json.dumps(list(users.keys()))
    res.set_data(res_body)
    return res

if __name__ == "__main__":
    app.debug = True
    app.run()