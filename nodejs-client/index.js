import fetch from "node-fetch";
import bcrypt from "bcryptjs";

/* unable to get TS working with different import/from
interface User {
    name: string,
    passwd: string,
    hash: string
};
*/

const serverPort = 5000;
const serverUrl = `http://localhost:${serverPort}/`;
// too lazy to do user input now
const user = {
    name: "lukas",
    passwd: "password123",
};

const main = async() => {
    await registerUser(user);
    await login({...user, passwd: "dajkfl"});
    await login(user);
};

const registerUser = async(user) => {
    const req = {
        method: "POST",
        headers: {
            contentType: "application/json"
        },
        body: JSON.stringify(user)
    };
    const response = await fetch(`${serverUrl}/user/register`, req);
};

const login = async(user) => {
    console.log(JSON.stringify(user))
    const req = {
        method: "POST",
        headers: {
            accept: "application/json",
            contentType: "application/json"
        },
        body: JSON.stringify(user)
    };
    const response = await fetch(`${serverUrl}/user/login`, req);   
    if (response) {
        const data = await response.text();
        console.log(data);
    }
};

main();